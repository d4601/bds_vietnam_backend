import React, { useState } from 'react'
import PropTypes from 'prop-types'
import matchSorter from 'match-sorter'
import { sortBy } from 'lodash'
import { FormattedMessage } from 'react-intl'

import LeftMenuLink from '../LeftMenuLink'
import LeftMenuLinkHeader from '../LeftMenuLinkHeader'
import LeftMenuListLink from './LeftMenuListLink'
import EmptyLinksList from './EmptyLinksList'
import EmptyLinksListWrapper from './EmptyLinksListWrapper'

const LeftMenuLinksSection = ({
  section,
  searchable,
  location,
  links,
  emptyLinksListMessage,
  shrink
}) => {
  const [search, setSearch] = useState('')

  if (
    links[0] &&
    links[0].schema &&
    links[0].schema.kind === 'collectionType'
  ) {
    for (let index = 0; index < links.length; index++) {
      // links[index].isDisplayed = false
      links[index].isCore = false
      if (
        links[index] &&
        links[index].schema &&
        links[index].schema.kind === 'collectionType'
      ) {
        if (links[index].name === 'tour') {
          links[index].label = 'Tours'
          // links[index].isDisplayed = true
          links[index].isCore = true
        } else if (links[index].name === 'deposit') {
          links[index].label = 'Payment Managment'
          // links[index].isDisplayed = true
          links[index].isCore = true
        } else if (links[index].uid === 'plugins::users-permissions.user') {
          links[index].label = 'Tour guide'
          // links[index].isDisplayed = true
          links[index].isCore = true
        } else if (links[index].name === 'blog') {
          links[index].label = 'Blogs'
          // links[index].isDisplayed = true
          links[index].isCore = true
        } else if (links[index].name === 'notification') {
          links[index].label = 'Notifications'
          // links[index].isDisplayed = true
          links[index].isCore = true
        } else {
          // links[index].isDisplayed = false
          links[index].isCore = false
        }
      }
    }
  }

  links &&
    links[0].schema &&
    links[0].schema.kind === 'collectionType' &&
    links.push({
      ...links[links.find((element) => element.name === 'user')],
      label: 'Tourist',
      // isDisplayed: true,
      isCore: true
    })

  let filteredList = sortBy(
    matchSorter(links, search, {
      keys: ['label']
    }),
    'label'
  )

  let contentType = []

  if (
    links[0] &&
    links[0].schema &&
    links[0].schema.kind === 'collectionType'
  ) {
    // filteredList = links.filter((item) => item.isDisplayed)
    filteredList = links.filter((item) => item.isCore)
    // contentType = links.filter((item) => !item.isDisplayed)
    contentType = links.filter((item) => !item.isCore)
  }

  const APP_HOST = process.env.APP_HOST || 'https://api.privatrip.com' || 'https://privatrip-api.tungtung.online.th'
  const APP_PAYMENT = process.env.APP_PAYMENT_ADMIN || 'https://payment-admin.privatrip.com' || 'https://privatrip-payment.tungtung.online.th'

  const getLinkDestination = (link) => {
    console.log('Des: ', link.destination)
    if (link.label === 'Tour guide') {
      return `${APP_HOST}/admin/plugins/content-manager/collectionType/plugins::users-permissions.user?roleName=TOUR_GUIDE`
    }

    if (link.label === 'Tourist') {
      return `${APP_HOST}/admin/plugins/content-manager/collectionType/plugins::users-permissions.user?roleName=TOURIST`
    }

    if (link.name === 'tour') {
      return `${APP_HOST}/admin/plugins/content-manager/collectionType/application::tour.tour`
    }

    if (link.name === 'blog') {
      return `${APP_HOST}/admin/plugins/content-manager/collectionType/application::blog.blog`
    }

    if (link.name === 'notification') {
      return `${APP_HOST}/admin/plugins/content-manager/collectionType/application::notification.notification`
    }

    if (link.name === 'deposit') {
      return APP_PAYMENT
    }

    if (['plugins', 'general'].includes(section)) {
      return link.destination
    }
    if (link.schema && link.schema.kind) {
      return `/plugins/${link.plugin}/${link.schema.kind}/${
        link.destination || link.uid
      }`
    }
    return `/plugins/${link.plugin}/${link.destination || link.uid}`
  }

  return (
    <>
      {section === 'collection_type' ? (
        <LeftMenuLinkHeader
          section='customTypes'
          searchable={searchable}
          setSearch={setSearch}
          search={search}
        />
      ) : (
        <LeftMenuLinkHeader
          section={section}
          searchable={searchable}
          setSearch={setSearch}
          search={search}
        />
      )}
      <LeftMenuListLink shrink={shrink}>
        {filteredList.length > 0 ? (
          filteredList.map((link, index) => {
            return (
              <LeftMenuLink
                location={location}
                // There is no id or unique value in the link object for the moment.
                // eslint-disable-next-line react/no-array-index-key
                key={index}
                iconName={link.icon}
                label={link.label}
                destination={getLinkDestination(link)}
              />
            )
          })
        ) : (
          <EmptyLinksListWrapper>
            <FormattedMessage
              id={emptyLinksListMessage}
              defaultMessage='No plugins installed yet'
            >
              {(msg) => <EmptyLinksList>{msg}</EmptyLinksList>}
            </FormattedMessage>
          </EmptyLinksListWrapper>
        )}
      </LeftMenuListLink>
      <hr />
      {contentType.length !== 0 ? (
        <LeftMenuLinkHeader
          section={section + 'System'}
          searchable={searchable}
          setSearch={setSearch}
          search={search}
        />
      ) : (
        <> </>
      )}
      {contentType.length !== 0 ? (
        contentType.map((link, index) => {
          return (
            <LeftMenuLink
              location={location}
              // There is no id or unique value in the link object for the moment.
              // eslint-disable-next-line react/no-array-index-key
              key={index}
              iconName={link.icon}
              label={link.label}
              destination={getLinkDestination(link)}
            />
          )
        })
      ) : (
        <></>
      )}
    </>
  )
}

LeftMenuLinksSection.defaultProps = {
  shrink: false
}

LeftMenuLinksSection.propTypes = {
  section: PropTypes.string.isRequired,
  searchable: PropTypes.bool.isRequired,
  shrink: PropTypes.bool,
  location: PropTypes.shape({
    pathname: PropTypes.string
  }).isRequired,
  links: PropTypes.arrayOf(PropTypes.object).isRequired,
  emptyLinksListMessage: PropTypes.string
}

LeftMenuLinksSection.defaultProps = {
  emptyLinksListMessage: 'components.ListRow.empty'
}

export default LeftMenuLinksSection
